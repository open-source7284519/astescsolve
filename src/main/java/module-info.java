module myModule {
    requires javafx.controls;
    requires javafx.fxml;

    opens airship;
    exports airship;
}